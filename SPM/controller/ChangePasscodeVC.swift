//
//  ChangePasscodeVC.swift
//  SPM
//
//  Created by leon.wee.yuan.liang on 29/9/21.
//

import UIKit

class ChangePasscodeVC: UIViewController {

    @IBOutlet var myTitle: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        myTitle.text = "Change Password"
        self.navigationItem.title = "Change Password"
        
    }
}
