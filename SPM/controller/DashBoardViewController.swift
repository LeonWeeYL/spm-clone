//
//  ViewController.swift
//  SPM
//
//  Created by leon.wee.yuan.liang on 27/9/21.
//

import UIKit

class DashBoardViewController: UIViewController {

    
    @IBOutlet var topImageView: UIView!
    @IBOutlet var digitalSignInView: UIView!
    @IBOutlet var EShortcutsView: UIView!
    
    @IBOutlet var CPFView: UIView!
    @IBOutlet var HealthHubView: UIView!
    @IBOutlet var IRASView: UIView!
    @IBOutlet var HDBView: UIView!
    @IBOutlet var myCareerView: UIView!
    @IBOutlet var MYICAView: UIView!

    
    @IBAction func btnGoToSetting(_ sender: Any) { }
    
    @IBAction func CPFBtn(_ sender: Any) {
        print("CPF")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
//      MARK: Navbar
        self.title = "Singpass"
        navigationController?.navigationBar.barTintColor = UIColor.lightGray
    
        topImageView.addDashedBorder()
        
        // MARK: Digital Sign in
        
        digitalSignInView.addDashedBorder()
        
//         MARK: E Shortcuts
        EShortcutsView.addGrayBorder()
        CPFView.addDashedBorder()
        HealthHubView.addDashedBorder()
        IRASView.addDashedBorder()
        HDBView.addDashedBorder()
        myCareerView.addDashedBorder()
        MYICAView.addDashedBorder()
        
    }


}



