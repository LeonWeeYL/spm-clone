//
//  SettingTableTableViewController.swift
//  SPM
//
//  Created by leon.wee.yuan.liang on 28/9/21.
//

import UIKit


struct Settings {
    var header:String
    var subHeader:[String]
    
    init (header:String, subHeader:[String]) {
        self.header = header
        self.subHeader = subHeader
    }
}


class SettingTableTableViewController: UITableViewController {

  

    
    var settingsList = [Settings]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Setting"
        
        
        // define data to each sections
        settingsList.append(Settings(header: "Account", subHeader: ["Change passcode","Enable Face ID"]))
        settingsList.append(Settings(header: "Help & Feedback", subHeader: ["Have an issue","Give us feedback"]))

    }

    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {

        return settingsList.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return settingsList[section].subHeader.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
               
        switch settingsList[indexPath.section].subHeader[indexPath.row] {
        
            // SettingToggleCell for "Enable Face ID"
            case "Enable Face ID":
                let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SettingToggleCell.self), for: indexPath) as! SettingToggleCell
                cell.settingLbl.text = settingsList[indexPath.section].subHeader[indexPath.row]
                return cell
    
            // else use SettingDefaultCell
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SettingDefaultCell.self), for: indexPath) as! SettingDefaultCell
                cell.settingLbl.text = settingsList[indexPath.section].subHeader[indexPath.row]
                return cell
        
        }
    }

    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return settingsList[section].header
    }
    
    
    // MARK: - Methods
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true) // deselect effect
        
    
        let settingName = settingsList[indexPath.section].subHeader[indexPath.row]
        
        switch settingName {
        case "Change passcode":
            let vc = storyboard?.instantiateViewController(withIdentifier: "ChangePasscodeVC") as! ChangePasscodeVC
            navigationController?.pushViewController(vc, animated: true)

            
        case "Have an issue":
            let vc = storyboard?.instantiateViewController(withIdentifier: "HaveAnIssueVC") as! HaveAnIssueVC
            navigationController?.pushViewController(vc, animated: true)

            
            
        case "Give us feedback":
            let vc = storyboard?.instantiateViewController(withIdentifier: "FeedBackVC") as! FeedBackVC
            navigationController?.pushViewController(vc, animated: true)

            
        default:
            return // for non clickable row
        }

   
    }
    
}

    

    


