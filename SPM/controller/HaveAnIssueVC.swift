//
//  HaveAnIssueVC.swift
//  SPM
//
//  Created by leon.wee.yuan.liang on 29/9/21.
//

import UIKit

class HaveAnIssueVC: UIViewController {

    
    @IBOutlet var myTitle: UILabel! {
        didSet {
            myTitle.font = UIFont.systemFont(ofSize: 20.0)
        }
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        myTitle.text = "Have An Issue?"
        self.navigationItem.title = "Have An Issue?"
    }


}
