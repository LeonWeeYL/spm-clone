//
//  helper.swift
//  SPM
//
//  Created by leon.wee.yuan.liang on 28/9/21.
//

import UIKit

class HelperFont {
    
    let Font20Light = UIFont.systemFont(ofSize: 20, weight: .light)
}


extension UIView {
        
//        func addDashedBorder() {
//                let border = CAShapeLayer()
//                border.strokeColor = UIColor.red.cgColor
//                border.lineWidth = 2
//                border.lineDashPattern = [6,3]
//                border.frame = bounds
//                border.fillColor = nil
//                border.path = UIBezierPath(roundedRect: bounds, byRoundingCorners: .allCorners, cornerRadii: CGSize(width: 20, height: 20)).cgPath
//
//                self.layer.addSublayer(border)
//            }
        
    func addDashedBorder() {
        
        let color = UIColor.black.cgColor
 
        
        let shapeLayer:CAShapeLayer = CAShapeLayer()
        let frameSize = self.frame.size
        let cornerRadius = frameSize.width/50
        let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)

        shapeLayer.bounds = shapeRect
        shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = color
        shapeLayer.lineWidth = 1
        shapeLayer.lineJoin = CAShapeLayerLineJoin.round
        shapeLayer.lineDashPattern = [6,3]
        shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: cornerRadius).cgPath
        
        print(cornerRadius)
        self.layer.addSublayer(shapeLayer)
    }
    
    func addGrayBorder() {
        let color = UIColor.lightGray.cgColor
        
        let shapeLayer:CAShapeLayer = CAShapeLayer()
        let frameSize = self.bounds
        let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)
        let cornerRadius = frameSize.width/50
        
        shapeLayer.bounds = shapeRect
        shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = color
        shapeLayer.lineWidth = 1
        shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: cornerRadius).cgPath
        
        self.layer.addSublayer(shapeLayer)
    }
    
}
