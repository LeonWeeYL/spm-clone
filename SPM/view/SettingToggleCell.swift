//
//  SettingToggleCell.swift
//  SPM
//
//  Created by leon.wee.yuan.liang on 28/9/21.
//

import UIKit

class SettingToggleCell: UITableViewCell {

    
    let helper =  HelperFont()
    
    @IBOutlet var settingLbl: UILabel! {
        didSet{
            settingLbl.font = helper.Font20Light
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
